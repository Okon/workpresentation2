﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.ViewModels
{
    public class DetailsViewModel
    {
        public int Id { get; set; }
        public int Age { get; set; }
        public int Height { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Description { get; set; }
        public string Description2 { get; set; }
        public string Description3 { get; set; }
        public string Description4 { get; set; }
        public int Description5 { get; set; }
    }
}
﻿using Application;
using AutoMapper;

namespace Api.Profiles
{
    public class DetailsProfiler : Profile
    {
        public DetailsProfiler()
        {
            CreateMap<DetailsViewModel, Details>()
                .ForMember(dest => dest.Description5, opt => opt.MapFrom(src => src.Description5.ToString()));
        }
    }
}

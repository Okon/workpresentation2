﻿using Application;
using Application.Interfaces;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace Api.Controllers
{
    public class HomeControllerInstaller: IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component
                .For<IHelper>()
                .ImplementedBy<Helper>()
                .LifestylePerWebRequest());

            container.Register(Component
                .For<IHomeManager>()
                .ImplementedBy<HomeManager>()
                .LifestylePerWebRequest());
        }
    }
}
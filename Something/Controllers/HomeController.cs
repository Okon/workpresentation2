﻿using Application;
using Application.Interfaces;
using AutoMapper;
using System.Web.Http;

namespace Api.Controllers
{
    public class HomeController : ApiController
    {
        private readonly IHomeManager _homeManager;
        private readonly IMapper _mapper;

        public HomeController(IHomeManager homeManager, IMapper mapper)
        {
            _homeManager = homeManager;
            _mapper = mapper;
        }

        [HttpPost]
        [Route("test1")]
        public string MyTest(DetailsViewModel detailsViewModel)
        {
            var details = _mapper.Map<DetailsViewModel,Details> (detailsViewModel);
            var result =_homeManager.GetHomeMessage(details);
            /*
             * 
             * 
             * 
             * 
             */
            return result;
        }

        [HttpPost]
        [Route("test2")]
        public string Test2(DetailsViewModel detailsViewModel)
        {

            var details = new Details()
            {
                Id = 0,
                Age = detailsViewModel.Age,
                Description = detailsViewModel.Description,
                Description2 = detailsViewModel.Description2,
                Description3 = detailsViewModel.Description3,
                Description4 = detailsViewModel.Description4,
                Description5 = detailsViewModel.Description5.ToString(),
                Height = detailsViewModel.Height,
                Name = detailsViewModel.Name,
                Surname = detailsViewModel.Surname
            };

            var result = _homeManager.GetHomeMessage(details);
            return result;
        }
    }
}
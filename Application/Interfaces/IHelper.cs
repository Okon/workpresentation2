﻿namespace Application.Interfaces
{
    public interface IHelper
    {
        MessageType GetType(Details model);
    }
}

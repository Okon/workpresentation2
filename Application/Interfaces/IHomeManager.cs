﻿namespace Application.Interfaces
{
    public interface IHomeManager
    {
        string GetHomeMessage(Details details);

        string GetStaticHomeMessage(Details details);
    }
}

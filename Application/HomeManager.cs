﻿using Application.Interfaces;
using System;
using System.IO;

namespace Application
{
    public class HomeManager : IHomeManager
    {
        private readonly IHelper _helper;

        public HomeManager(IHelper helper)
        {
            _helper = helper;
        }

        public string GetHomeMessage(Details details)
        {
            var value = _helper.GetType(details);
            if(value == MessageType.Number2)
            {
                throw new IOException("MyException");
            }

            return "The status is OK";
        }

        public string GetStaticHomeMessage(Details details)
        {

            var value = StaticHelper.GetType(details);
            if(value == MessageType.Number2)
            {
                throw new IOException("MyException");
            }

            return "The status is OK";
        }
    }
}

﻿using Application;
using Application.Interfaces;
using Moq;
using NUnit.Framework;
using System.IO;

namespace Tests.Application
{
    [TestFixture]
    public class HomeManagerTests
    {
        private HomeManager _homeManager;
        private Mock<IHelper> _helperMock;

        [SetUp]
        public void SetUp()
        {
            _helperMock = new Mock<IHelper>();
            _homeManager = new HomeManager(_helperMock.Object);
        }

        [Test]
        public void GetHomeMessage_ReceivedNumber2Message_ThrowsIOException()
        {
            _helperMock.Setup(m => m.GetType(It.IsAny<Details>())).Returns(MessageType.Number2);

            Assert.Throws<IOException>(() => _homeManager.GetHomeMessage(It.IsAny<Details>()));
        }

        [Test]
        public void GetStaticHomeMessage_ReceivedNumber2Message_ThrowsException()
        {
            //var details = new Details();

            //var details = new Details()
            //{
            //    Age = 10,
            //    Description = "s",
            //    Description2 = "s",
            //    Description3 = "s",
            //    Description5 = "s",
            //    Name = "s"
            //};

            var details = new Details()
            {
                Age = 3,
                Description = "s",
                Description2 = "s",
                Description3 = "s",
                Description5 = "s",
                Name = "s"
            };


            var results = _homeManager.GetStaticHomeMessage(details);

            Assert.Throws<IOException>(() => _homeManager.GetStaticHomeMessage(It.IsAny<Details>()));
        }
    }
}

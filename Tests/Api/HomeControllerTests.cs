﻿using Moq;
using NUnit.Framework;
using Api.Controllers;
using Application;
using AutoMapper;
using Application.Interfaces;
using Api;

namespace Tests.Api
{
    [TestFixture]
    public class HomeControllerTests
    {
        private HomeController _homeController;
        private Mock<IHomeManager> _homeManagerMock;
        private Mock<IMapper> _mapperMock;

        [SetUp]
        public void SetUp()
        {
            _homeManagerMock = new Mock<IHomeManager>();
            _mapperMock = new Mock<IMapper>();
            _homeController = new HomeController(_homeManagerMock.Object, _mapperMock.Object);
        }

        [Test]
        public void Test_PositivePath_ReturnsReceivedMessage ()
        {
            var expectedMessage = "Text";
            _homeManagerMock.Setup(m => m.GetHomeMessage(It.IsAny<Details>())).Returns(expectedMessage);

            var results = _homeController.MyTest(It.IsAny<DetailsViewModel>());

            Assert.AreEqual(expectedMessage, results);
        }
    }
}
